%% RNATimeEvolution
clc
clear variables
close all


%% constants

% active velocity [0, 1]
uR = 0.000921/2;

% diffusion constant [0, 10]
DR = 7.09*10^-2/2;

% transcription rate [0, 1]
betaR = 0.0109;

% half-flie [0,24]
LifTR = 16 * 60 * 60;

%% some parameters
kR = log(2) / LifTR;
muR = uR / (2 * DR);
etaR = -kR - uR^2 / 4*DR;
lambdaR = (sqrt(uR^2 + 4*kR * DR) - uR) / 2*DR;
dR = kR / lambdaR;

xmax = 1/abs(lambdaR) * 3;
tmax = LifTR * 5;

%% grid space
x = linspace(0.1, xmax, 100)';
t = linspace(0.1, tmax, 100);

%% result
P = betaR/(2*dR) * ...
    exp(muR*x) * ...
    (-exp(-dR*t) * (exp(sqrt((-etaR - dR)/DR)*x) * ...
    erfc(sqrt(-dR - etaR)*sqrt(t) + x./(2*sqrt(DR*t))) + ...
    exp(-sqrt((-etaR - dR)/DR)*x) * erfc(-sqrt(-dR - etaR) * ...
    sqrt(t) + x./(2*sqrt(DR*t)))) + ...
    (exp(sqrt((-etaR)/DR)*x) * erfc(sqrt(-etaR) * ...
    sqrt(t) + x/(2*sqrt(DR*t))) + exp(-sqrt((-etaR)/DR)*x) * ...
    erfc(-sqrt(-etaR)*sqrt(t) + x/(2*sqrt(DR*t)))));
