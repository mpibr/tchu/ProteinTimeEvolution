%% ProteinTimeEvolution
clc
clear variables
close all

%% mRNA variables
%%% active velocity [0, 1]
uR = 0.000921 / 2;

%%% diffusion constant [0, 10]
DR = (7.09 * 10^-2) / 2;

%%% transcription rate [0, 1]
betaR = 0.0109;

%%% half-life [0, 24] hours
LifTR = 16 * 60 * 60; % [sec]

%% Protein variables
%%% active velocity [0, 1]
uP = 0.002814925197205756 / 2;

%%% diffusion constant [0, 10]
DP = 3.1276946635619507 / 2;

%%% translation rate
betaP = 0.02301;

%%% half-life [0, 10] days
LifTP = 6.6 * 24 * 60 * 60; % [sec]

%% Helper variables
kR = log(2) / LifTR;
kP = log(2) / LifTP;

muR = uR / (2 * DR);
etaR = kR + uR^2 / (4 * DR);
lambdaR = (sqrt(uR^2 + 4 * kR * DR) - uR) / (2 * DR);
dR = kR / lambdaR;

muP = uP / (2 * DP);
etaP = kP + uP^2 / (4 * DP);
lambdaP = (sqrt(uP^2 + 4 * kP * DP) - uP) / (2 * DP);
dP = kP * (DP * lambdaR^2 + uP * lambdaR - kP) / (DP * lambdaP * lambdaR + uP * lambdaP - kP);

C1 = 1 / (etaP - DP * (lambdaR + muP)^2);
C4 = 1 / dP - C1;

%% Modelfunction
%{
P = @(t, x) betaP * betaR / dR * ...
    (1/(2*dP) .* ...
    exp(-lambdaP.*x) .* ...
    erfc(x./(2*sqrt(DP.*t)) - sqrt(etaP.*t)) + ...
    1/(kP-DP*lambdaR^2-lambdaR*uP) .* ...
    (exp(-lambdaR.*x) - ...
    1/2 .* exp(-lambdaP.*x) .* erfc(x./(2/sqrt(DP.*t)) ...
    -sqrt(etaP.*etaP))) +...
    C1/2 .* exp(-1/C1.*t + 2*muP.*x + lambdaR.*x) .* ...
    erfc(x ./ (2*sqrt(DP.*t)) + (lambdaR+muP).* sqrt(DP.*t)) + ...
    C4/2 .* exp((muP + sqrt(etaP/DP)) .* x) .* ...
    erfc(x ./ (2 .* sqrt(DP.*t)) + sqrt(etaP .* t)) + ...
    C1*(-exp(-1/C1.*t-lambdaR.*x)+1/2*exp(-1/C1.*t-lambdaR.*x) .*...
    erfc(x./(2*sqrt(DP.*t)) + real(sqrt((etaP - dP).*t)))+... %%% ERROR in SQRT
    real(exp(-x.*sqrt((etaP - dP)/DP))) .*... %%% ERROR in SQRT
    erfc(x./(2*sqrt(DP.*t)) - real(sqrt((etaP - dP).*t))))); %%% ERROR in SQRT
%}

%% calculate Model
t = linspace(0, 1728000, 1000)';
x = linspace(0, 300, 1000)';
Z = P_YF(t, x); % Yombe debugging

%% plot Model
%{
whos X T Z;
figure('color', 'w');
if size(Z,2) == 1
    plot(Z);
else
    imagesc(Z);
end
%}

%% plot contour
% Yombe debugging 

[X, T] = meshgrid(x, t);
figure('color','w');
contourf(X, T, Z, 10);
yticks([1*24*3600 5*24*3600 10*24*3600 15*24*3600 20*24*3600])
yticklabels({'1 day', '5 days', '10 days', '15 days', '20 days'})
%}
