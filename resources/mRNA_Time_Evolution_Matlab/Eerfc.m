function Eerfc = Eerfc(a,b,c)
if isreal(b)
    Eerfc=exp(2*a*b)*erfc(a*c+b/c)+exp(-2*a*b)*erfc(a*c-b/c);
else
Eerfc=real(exp(2*a*b)*(1-e(a*c+b/c))+exp(-2*a*b)*(1-e(a*c-b/c)));
end
