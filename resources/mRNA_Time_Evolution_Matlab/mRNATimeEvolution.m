%% ProteinTimeEvolution
clc
clear variables
close all


%% plot contour
t = linspace(0, 1*24*3600, 101)';
x = linspace(0, 100, 101)';
Z = mRNA(t,x); 

[X, T] = meshgrid(x, t);
figure('color','w');
contourf(X, T, Z, 10);
%yticks([1*24*3600 5*24*3600 10*24*3600 15*24*3600 20*24*3600])
%yticklabels({'1 day', '5 days', '10 days', '15 days', '20 days'})

