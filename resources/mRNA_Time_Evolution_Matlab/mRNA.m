function mRNA = mRNA(t, x)

%% mRNA variables
%%% active velocity [0, 1]
uR = 0.000921 / 2;

%%% diffusion constant [0, 10]
DR = (7.09 * 10^-2) / 2;

%%% transcription rate [0, 1]
betaR = 0.0109;

%%% half-life [0, 24] hours
LifTR = 16 * 60 * 60; % [sec]

%% Helper variables
kR = log(2) / LifTR;
muR = uR / (2 * DR);
etaR = kR + uR^2 / (4 * DR);
lambdaR = (sqrt(uR^2 + 4 * kR * DR) - uR) / (2 * DR);
dR = kR / lambdaR;

mRNA = zeros(numel(t), numel(x));

for i = 1:numel(x)
    for j = 1:numel(t)      
        % array multiplication is not necessary 
        mRNA(j,i) = betaR * exp(muR * x(i)) / (2 * dR) * ...
            ( - exp(-dR * t(j)) * Eerfc(x(i)/(2 * sqrt(DR)),sqrt(etaR - dR),1/sqrt(t(j))) + ...
            Eerfc(x(i)/(2 * sqrt(DR)),sqrt(etaR),1/sqrt(t(j))));
    end
end

end