function protein = P_YF(t, x)

%% mRNA variables
%%% active velocity [0, 1]
uR = 0.000921 / 2;

%%% diffusion constant [0, 10]
DR = (7.09 * 10^-2) / 2;

%%% transcription rate [0, 1]
betaR = 0.0109;

%%% half-life [0, 24] hours
LifTR = 16 * 60 * 60; % [sec]

%% Protein variables
%%% active velocity [0, 1]
uP = 0.002814925197205756 / 2;

%%% diffusion constant [0, 10]
DP = 3.1276946635619507 / 2;

%%% translation rate
betaP = 0.02301;

%%% half-life [0, 10] days
LifTP = 6.6 * 24 * 60 * 60; % [sec]

%% Helper variables
kR = log(2) / LifTR;
kP = log(2) / LifTP;

muR = uR / (2 * DR);
etaR = kR + uR^2 / (4 * DR);
lambdaR = (sqrt(uR^2 + 4 * kR * DR) - uR) / (2 * DR);
dR = kR / lambdaR;

muP = uP / (2 * DP);
etaP = kP + uP^2 / (4 * DP);
lambdaP = (sqrt(uP^2 + 4 * kP * DP) - uP) / (2 * DP);
dP = kP * (DP * lambdaR^2 + uP * lambdaR - kP) / (DP * lambdaP * lambdaR + uP * lambdaP - kP);

C1 = 1 / (etaP - DP * (lambdaR + muP)^2);
C4 = 1 / dP - C1;

protein = zeros(numel(t), numel(x));

for i = 1:numel(x)
    for j = 1:numel(t)
        
        protein(j,i) = betaP.*betaR./dR.*(1/(2*dP).*exp(-lambdaP.*x(i))...
            .*erfc(x(i)/(2.*sqrt(DP*t(j)))-sqrt(t(j).*etaP))+1/(kP-DP.*...   
            lambdaR.^2-lambdaR.*uP).*(exp(-lambdaR.*x(i))-1/2.*exp(-lambdaP...
            .*x(i)).*erfc(x(i)/(2.*sqrt(DP.*t(j)))-sqrt(t(j).*etaP)))+C1/2.*...
            exp(-1/C1.*t(j)+2.*muP.*x(i)+lambdaR.*x(i)).*erfc(x(i)/(2.*sqrt(... 
            DP.*t(j)))+(lambdaR+muP).*sqrt(t(j).*DP))+C4/2.*exp((muP+sqrt(...
            etaP/DP)).*x(i)).*erfc(x(i)/(2.*sqrt(DP.*t(j)))+sqrt(t(j).*etaP))... 
            +C1.*(-exp(-1/C1.*t(j)-lambdaR.*x(i))+1/2.*exp(-1/C1.*t(j)-...
            lambdaR.*x(i)).*erfc(x(i)/(2*sqrt(DP.*t(j)))-(lambdaR+muP).*...
            sqrt(t(j).*DP)))-1/(2.*dP).*exp(-dP.*t(j)+muP.*x(i)).*...
            (exp(x(i).*real(sqrt((etaP-dP)/DP))).*erfc(x(i)/(2.*sqrt(DP...
            .*t(j)))+real(sqrt(t(j).*(etaP-dP))))+exp(-x(i).*real(sqrt((etaP-dP)/DP))).*erfc(x(i)/(2 ...
            .*sqrt(DP.*t(j)))-real(sqrt(t(j).*(etaP-dP))))));
        
    end
end



end